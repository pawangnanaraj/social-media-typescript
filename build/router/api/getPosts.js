"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPostRouter = void 0;
const express_1 = __importDefault(require("express"));
const auth_1 = require("../../middleware/auth");
const Posts_1 = require("../../models/Posts");
exports.getPostRouter = express_1.default.Router();
// @route    GET api/posts
// @desc     Get all posts
// @access   Private
exports.getPostRouter.get('/', auth_1.authMiddleware, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const posts = yield Posts_1.PostModel.find().sort({ createdAt: -1 });
        res.json(posts);
    }
    catch (error) {
        console.error(error.message);
        res.status(500).send('Server Error');
    }
}));
// @route    GET api/posts/:id
// @desc     Get post by ID
// @access   Private
exports.getPostRouter.get('/:id', auth_1.authMiddleware, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield Posts_1.PostModel.findById(req.params.id);
        //check if post is found
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' });
        }
        res.json(post);
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}));

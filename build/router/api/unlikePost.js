"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.unlikePostRouter = void 0;
const express_1 = __importDefault(require("express"));
const auth_1 = require("../../middleware/auth");
const Posts_1 = require("../../models/Posts");
exports.unlikePostRouter = express_1.default.Router();
// @route    PUT api/posts/unlike/:id
// @desc     Unlike a post
// @access   Private
exports.unlikePostRouter.put('/unlike/:id', auth_1.authMiddleware, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield Posts_1.PostModel.findById(req.params.id);
        console.log('testing');
        // Check if the post has not yet been liked by the user
        if (post.likes.filter((like) => like.user.toString() === req.body.user.user.id).length === 0) {
            return res.status(400).json({ msg: 'Post has not yet been liked' });
        }
        //console.log(post.likes);
        console.log(post.likes.filter((like) => like.user.toString() === req.body.user.user.id).length);
        // console.log(req.body.user.user.id);
        // console.log(post.likes);
        //   //get the removed index
        const removeIndex = post.likes
            .map((like) => like.user.toString())
            .indexOf(req.body.user.user.id);
        //remove one like from the given index
        post.likes.splice(removeIndex, 1);
        yield post.save();
        return res.json(post.likes);
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}));

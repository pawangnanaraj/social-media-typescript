"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
const express_validator_1 = require("express-validator");
const auth_1 = require("../../middleware/auth");
const Posts_1 = require("../../models/Posts");
const User_1 = require("../../models/User");
// @route    POST api/posts
// @desc     Create a post
// @access   Private
router.post('/', auth_1.authMiddleware, (0, express_validator_1.check)('text', 'Text is required').notEmpty(), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    try {
        const userDetails = yield User_1.UserModel.findById(req.body.user.user.id).select('-password');
        const newPost = new Posts_1.PostModel({
            text: req.body.text,
            name: userDetails.name,
            user: req.body.user.user.id,
        });
        const post = yield newPost.save();
        res.json(post);
    }
    catch (error) {
        console.error(error.message);
        res.status(500).send('Server Error');
    }
}));
// @route    GET api/posts
// @desc     Get all posts
// @access   Private
router.get('/', auth_1.authMiddleware, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const posts = yield Posts_1.PostModel.find().sort({ date: -1 });
        res.json(posts);
    }
    catch (error) {
        console.error(error.message);
        res.status(500).send('Server Error');
    }
}));
// @route    GET api/posts/:id
// @desc     Get post by ID
// @access   Private
router.get('/:id', auth_1.authMiddleware, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield Posts_1.PostModel.findById(req.params.id);
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' });
        }
        res.json(post);
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}));
// @route    DELETE api/posts/:id
// @desc     Delete a post
// @access   Private
router.delete('/:id', auth_1.authMiddleware, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield Posts_1.PostModel.findById(req.params.id);
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' });
        }
        // Check user
        if (post.user.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'User not authorized' });
        }
        yield post.remove();
        res.json({ msg: 'Post removed' });
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}));
// @route    PUT api/posts/like/:id
// @desc     Like a post
// @access   Private
router.put('/like/:id', auth_1.authMiddleware, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield Posts_1.PostModel.findById(req.params.id);
        // Check if the post has already been liked
        if (post.likes.filter((like) => like.user.toString() === req.user.id).length >
            0) {
            return res.status(400).json({ msg: 'Post already liked' });
        }
        post.likes.unshift({ user: req.user.id });
        yield post.save();
        return res.json(post.likes);
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}));
// @route    PUT api/posts/unlike/:id
// @desc     Unlike a post
// @access   Private
router.put('/unlike/:id', auth, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield Post.findById(req.params.id);
        // Check if the post has not yet been liked
        if (post.likes.filter((like) => like.user.toString() === req.user.id)
            .length === 0) {
            return res.status(400).json({ msg: 'Post has not yet been liked' });
        }
        // remove the like
        post.likes = post.likes.filter(({ user }) => user.toString() !== req.user.id);
        //get the removed index
        const removeIndex = post.likes
            .map((like) => like.user.toString())
            .indexOf(req.user.id);
        //remove one like from the given index
        post.likes.splice(removeIndex, 1);
        yield post.save();
        return res.json(post.likes);
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}));
// @route    POST api/posts/comment/:id
// @desc     Comment on a post
// @access   Private
router.post('/comment/:id', auth, (0, express_validator_1.check)('text', 'Text is required').notEmpty(), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    try {
        const user = yield User.findById(req.user.id).select('-password');
        const post = yield Post.findById(req.params.id);
        const newComment = {
            text: req.body.text,
            name: user.name,
            user: req.user.id,
        };
        post.comments.unshift(newComment);
        yield post.save();
        res.json(post.comments);
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}));
// @route    DELETE api/posts/comment/:id/:comment_id
// @desc     Delete comment
// @access   Private
router.delete('/comment/:id/:comment_id', auth, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const post = yield Post.findById(req.params.id);
        // Pull out comment
        const comment = post.comments.find((comment) => comment.id === req.params.comment_id);
        // If comment does not exist
        if (!comment) {
            return res.status(404).json({ msg: 'Comment does not exist' });
        }
        // Check user
        if (comment.user.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'User not authorized' });
        }
        //get the removed index
        const removeIndex = post.comments
            .map((comment) => comment.user.toString())
            .indexOf(req.user.id);
        console.log(removeIndex);
        //remove one comment at the given index
        post.comments.splice(removeIndex, 1);
        yield post.save();
        return res.json(post.comments);
    }
    catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }
}));
module.exports = router;

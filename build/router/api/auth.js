"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authRouter = void 0;
const express_1 = __importDefault(require("express"));
const auth_1 = require("../../middleware/auth");
const User_1 = require("../../models/User");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const config_1 = __importDefault(require("config"));
const express_validator_1 = require("express-validator");
exports.authRouter = express_1.default.Router();
// route --> get api/auth
// access --> public
//Authenticate the user and return the id
//this is a protected route
//console.log(auth);
exports.authRouter.get('/', auth_1.authMiddleware, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //get only id and leave the password
        const userId = yield User_1.UserModel.findById(req.body.user.user.id).select('-password');
        //console.log(userId);
        res.json(userId);
    }
    catch (error) {
        console.error(error);
        res.sendStatus(500).send('Server Error');
    }
}));
// route --> post api/authts
// access --> public
//Authenticate the user and login
//this is a protected route
exports.authRouter.post('/', [
    (0, express_validator_1.body)('email', 'Please enter a valid email').isEmail(),
    (0, express_validator_1.body)('password', 'password id required').exists(),
], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    //see if the user exits, send an error
    const { email, password } = req.body;
    try {
        let user = yield User_1.UserModel.findOne({ email });
        if (!user) {
            return res.status(400).json({ errors: [{ msg: 'User not Found' }] });
        }
        //verify password
        const passwordMatch = yield bcrypt_1.default.compare(password, user.password);
        if (!passwordMatch) {
            return res.status(400).json({ msg: 'Invalid Credentials' });
        }
        //return the JWT
        const payload = {
            user: {
                id: user.id,
            },
        };
        const token = jsonwebtoken_1.default.sign(payload, config_1.default.get('jwtSecret'), {
            expiresIn: 360000,
        });
        res.json({ token: token });
    }
    catch (error) {
        console.error(error);
        return res.status(500).send('server error');
    }
}));

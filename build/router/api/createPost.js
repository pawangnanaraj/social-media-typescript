"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createPostRouter = void 0;
const express_1 = __importDefault(require("express"));
const express_validator_1 = require("express-validator");
const auth_1 = require("../../middleware/auth");
const Posts_1 = require("../../models/Posts");
const User_1 = require("../../models/User");
exports.createPostRouter = express_1.default.Router();
// @route    POST api/posts
// @desc     Create a post
// @access   Private
exports.createPostRouter.post('/', auth_1.authMiddleware, (0, express_validator_1.check)('text', 'Text is required').notEmpty(), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    try {
        const userDetails = yield User_1.UserModel.findById(req.body.user.user.id).select('-password');
        const newPost = new Posts_1.PostModel({
            text: req.body.text,
            name: userDetails === null || userDetails === void 0 ? void 0 : userDetails.name,
            user: req.body.user.user.id,
        });
        yield newPost.save();
        res.json(newPost);
    }
    catch (error) {
        console.error(error.message);
        res.status(500).send('Server Error');
    }
}));

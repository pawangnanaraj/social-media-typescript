"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerUserRouter = void 0;
const express_1 = __importDefault(require("express"));
const User_1 = require("../../models/User");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const config_1 = __importDefault(require("config"));
const express_validator_1 = require("express-validator");
exports.registerUserRouter = express_1.default.Router();
// route --> POST  api/users
//register user
exports.registerUserRouter.post('/', [
    (0, express_validator_1.body)('name', 'Name is required').not().isEmpty(),
    (0, express_validator_1.body)('email', 'Please enter a valid email').isEmail(),
    (0, express_validator_1.body)('password', 'please enter a valid with 6 or more alphanumeric value')
        .isLength({ min: 6 })
        .isAlphanumeric()
        .isAlphanumeric(),
], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const { name, email, password } = req.body;
    try {
        let user = yield User_1.UserModel.findOne({ email });
        //see if the user exits, send an error
        if (user) {
            return res
                .sendStatus(400)
                .json({ errors: [{ msg: 'user already exists' }] });
        }
        else {
            user = new User_1.UserModel({
                name,
                email,
                password,
            });
            //encrypt the password
            const salt = yield bcrypt_1.default.genSalt(10);
            user.password = yield bcrypt_1.default.hash(password, salt);
            yield user.save();
            //return the JWT
            const payload = {
                user: {
                    id: user.id,
                },
            };
            const token = jsonwebtoken_1.default.sign(payload, config_1.default.get('jwtSecret'), {
                expiresIn: 360000,
            });
            res.json({ token: token });
        }
    }
    catch (error) {
        console.error(error.message);
        return res.sendStatus(500).send('server error');
    }
}));

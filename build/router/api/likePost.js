"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.likePostRouter = void 0;
const express_1 = __importDefault(require("express"));
const auth_1 = require("../../middleware/auth");
const Posts_1 = require("../../models/Posts");
exports.likePostRouter = express_1.default.Router();
// @route    PUT api/posts/like/:id
// @desc     Like a post
// @access   Private
exports.likePostRouter.put('/like/:id', auth_1.authMiddleware, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //get the post by id
        const post = yield Posts_1.PostModel.findById(req.params.id);
        // Check if the post has already been liked
        if (post.likes.filter((like) => like.user.toString() === req.body.user.user.id).length > 0) {
            return res.status(400).json({ msg: 'Post already liked' });
        }
        post.likes.unshift({ user: req.body.user.user.id });
        yield post.save();
        return res.json(post.likes);
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}));

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const connectMongoDB_1 = require("./connectMongoDB");
const auth_1 = require("./router/api/auth");
const user_1 = require("./router/api/user");
const createPost_1 = require("./router/api/createPost");
const getPosts_1 = require("./router/api/getPosts");
const deletePost_1 = require("./router/api/deletePost");
const likePost_1 = require("./router/api/likePost");
const unlikePost_1 = require("./router/api/unlikePost");
const addComment_1 = require("./router/api/addComment");
const cors_1 = __importDefault(require("cors"));
//initialise our app
const app = (0, express_1.default)();
//connect to DB
(0, connectMongoDB_1.connectDB)();
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.get('', (req, res) => res.send('API is working'));
app.use('/api/auth', auth_1.authRouter);
app.use('/api/users', user_1.registerUserRouter);
//post APIs
app.use('/api/post', createPost_1.createPostRouter);
app.use('/api/post', getPosts_1.getPostRouter);
app.use('/api/post', deletePost_1.deletePostRouter);
app.use('/api/post', likePost_1.likePostRouter);
app.use('/api/post', unlikePost_1.unlikePostRouter);
app.use('/api/comment', addComment_1.addCommentRouter);
app.listen(5000, () => console.log('server running'));

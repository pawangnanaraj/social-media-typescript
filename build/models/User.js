"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModel = void 0;
const mongoose_1 = require("mongoose");
// 2. Create a Schema corresponding to the User interface.
const UserSchema = new mongoose_1.Schema({
    name: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
}, { timestamps: true }
//date: { type: Date, default(): Date.now; },
);
// 3. Create a Model.
exports.UserModel = (0, mongoose_1.model)('User', UserSchema);
//module.exports = mongoose.model('user', UserSchema);

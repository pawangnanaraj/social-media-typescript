"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostModel = void 0;
const mongoose_1 = require("mongoose");
// 2. Create a Schema corresponding to the Post interface.
const PostSchema = new mongoose_1.Schema({
    user: {
        type: mongoose_1.Schema.Types.ObjectId,
    },
    text: {
        type: String,
        required: true,
    },
    name: {
        type: String,
    },
    likes: [
        {
            user: {
                type: mongoose_1.Schema.Types.ObjectId,
            },
        },
    ],
    comments: [
        {
            user: {
                type: mongoose_1.Schema.Types.ObjectId,
            },
            text: {
                type: String,
                required: true,
            },
            name: {
                type: String,
            },
        },
        { timestamps: true },
    ],
}, { timestamps: true });
// 3. Create a Model.
exports.PostModel = (0, mongoose_1.model)('Posts', PostSchema);

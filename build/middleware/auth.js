"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authMiddleware = void 0;
const jwt = __importStar(require("jsonwebtoken"));
const config_1 = __importDefault(require("config"));
const authMiddleware = (req, res, next) => {
    //get token from header
    const token = req.header('x-auth-token');
    //check if no token
    if (!token) {
        return res.sendStatus(401).json({ msg: 'No token, authotization denied' });
    }
    //if there is a token, verify the token
    try {
        const decoded = jwt.verify(token, config_1.default.get('jwtSecret'));
        req.body.user = decoded;
        //console.info(decoded);
        //console.log(`req-body-user : ${req.body.user}`);
        //console.log(req.body.user.user.id);
        next();
    }
    catch (err) {
        res.sendStatus(401).json({ msg: 'Token is not valid' });
    }
};
exports.authMiddleware = authMiddleware;

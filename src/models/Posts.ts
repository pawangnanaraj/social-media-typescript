import { Schema, model } from 'mongoose';
import Post from '../interfaces/Post';
// 1. Create an interface representing a Post in MongoDB.
// interface Post {
//   user: Schema.Types.ObjectId;
//   text: string;
//   name: string;
//   likes: [{ user: Schema.Types.ObjectId }];
//   comments: [
//     {
//       user: Schema.Types.ObjectId;
//       text: string;
//       name: string;
//       id?: string;
//     }
//   ];
// }

// 2. Create a Schema corresponding to the Post interface.
const PostSchema = new Schema<Post>(
  {
    user: {
      type: Schema.Types.ObjectId,
    },
    text: {
      type: String,
      required: true,
    },
    name: {
      type: String,
    },
    likes: [
      {
        user: {
          type: Schema.Types.ObjectId,
        },
      },
    ],
    comments: [
      {
        user: {
          type: Schema.Types.ObjectId,
        },
        text: {
          type: String,
          required: true,
        },
        name: {
          type: String,
        },
      },
      { timestamps: true },
    ],
  },
  { timestamps: true }
);

// 3. Create a Model.
export const PostModel = model<Post>('Posts', PostSchema);

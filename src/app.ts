import express, { Application, Request, Response } from 'express';
import { connectDB } from './connectMongoDB';
import { authRouter } from './router/api/auth';
import { registerUserRouter } from './router/api/user';
import { postRouter } from './router/api/post';
import { profileRouter } from './router/api/profile';

import cors from 'cors';
//initialise our app
const app: Application = express();

//connect to DB
connectDB();

app.use(cors());
app.use(express.json());

app.get('', (req: Request, res: Response) => res.send('API is working'));

app.use('/api/auth', authRouter);
app.use('/api/users', registerUserRouter);
//post APIs
app.use('/api/post', postRouter);

app.use('/api/profile/', profileRouter);

app.listen(5000, () => console.log('server running'));

import express, { Request, Response } from 'express';
import { check, validationResult } from 'express-validator';
import { authMiddleware } from '../../middleware/auth';
import { PostModel } from '../../models/Posts';
import { UserModel } from '../../models/User';

export const postRouter = express.Router();

// @route    POST api/posts
// @desc     Create a post
// @access   Private
postRouter.post(
  '/',
  authMiddleware,
  check('text', 'Text is required').notEmpty(),
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const userDetails = await UserModel.findById(
        req.body.user.user.id
      ).select('-password');

      const newPost = new PostModel({
        text: req.body.text,
        name: userDetails?.name,
        user: req.body.user.user.id,
      });

      await newPost.save();

      res.json(newPost);
    } catch (error: any) {
      console.error(error.message);
      res.status(500).send('Server Error');
    }
  }
);

//route PUT /api/post/edit/:id
//edit a post
//access private
postRouter.put(
  '/edit/:id',
  authMiddleware,
  check('text', 'a text is required').notEmpty(),
  async (req: Request, res: Response) => {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      return res.status(400).json({ errors: validationErrors.array() });
    }
    try {
      const post = await PostModel.findById(req.params.id);

      //find it the post exists
      if (!post) {
        res.status(400).json({ msg: 'Post is not available' });
      }

      //check is the user is authorised
      const authorisedUser = post.user.toString();

      if (authorisedUser !== req.body.user.user.id) {
        res.status(404).json({ msg: 'User is not authorzied' });
      }
      //create the updated post
      post.text = req.body.text;

      //save the post
      await post.save();
      return res.json(post);
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route    GET api/posts
// @desc     Get all posts
// @access   Private
postRouter.get('/', authMiddleware, async (req: Request, res: Response) => {
  try {
    const posts = await PostModel.find().sort({ createdAt: -1 });
    res.json(posts);
  } catch (error: any) {
    console.error(error.message);
    res.status(500).send('Server Error');
  }
});

// @route    GET api/posts/:id
// @desc     Get post by ID
// @access   Private
postRouter.get('/:id', authMiddleware, async (req: Request, res: Response) => {
  try {
    const post = await PostModel.findById(req.params.id);
    //check if post is found
    if (!post) {
      return res.status(404).json({ msg: 'Post not found' });
    }

    res.json(post);
  } catch (err: any) {
    console.error(err.message);

    res.status(500).send('Server Error');
  }
});

// @route    DELETE api/posts/:id
// @desc     Delete a post
// @access   Private
postRouter.delete(
  '/:id',
  authMiddleware,
  async (req: Request, res: Response) => {
    try {
      const post = await PostModel.findById(req.params.id);

      if (!post) {
        return res.status(404).json({ msg: 'Post not found' });
      }

      // Check user
      if (post.user.toString() !== req.body.user.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }

      await post.remove();

      res.json({ msg: 'Post removed' });
    } catch (err: any) {
      console.error(err.message);

      res.status(500).send('Server Error');
    }
  }
);

// @route    POST api/posts/comment/:postid
// @desc     Create a comment on a post
// @access   Private
postRouter.post(
  '/:postId',
  authMiddleware,
  check('text', 'Text is required').notEmpty(),
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const user = await UserModel.findById(req.body.user.user.id).select(
        '-password'
      );
      //Find the post
      const post = await PostModel.findById(req.params.postId);

      const newComment = {
        text: req.body.text,
        name: user.name,
        user: req.body.user.user.id,
      };

      post.comments.unshift(newComment);
      await post.save();

      res.json(post.comments);
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

//route PUT /api/comment/edit/:postId/:comment_id
//edit a comment
//access private

postRouter.put(
  '/:postId/:comment_id',
  authMiddleware,
  check('text', 'a text is required').notEmpty(),
  async (req: Request, res: Response) => {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      return res.status(400).json({ errors: validationErrors.array() });
    }
    try {
      const post = await PostModel.findById(req.params.postId);

      //get the comment
      const comment = post.comments.find(
        (comment) => comment.id === req.params.comment_id
      );

      //find it the comment exists
      if (!comment) {
        res.status(400).json({ msg: 'comment is not available' });
      }
      console.log(comment.text);

      //check is the user is authorised
      const authorisedUser = comment.user.toString();

      if (authorisedUser !== req.body.user.user.id) {
        res.status(404).json({ msg: 'User is not authorzied' });
      }

      //create the updated comment
      comment.text = req.body.text;

      //save the comment along with the post
      await post.save();
      return res.json(comment);
    } catch (err: any) {
      console.error(err.message);
      res.status(500).json({ msg: 'Server Error' });
    }
  }
);

// @route    DELETE api/posts/comment/:id/:comment_id
// @desc     Delete comment
// @access   Private
postRouter.delete(
  '/:postId/:comment_id',
  authMiddleware,
  async (req: Request, res: Response) => {
    try {
      const post = await PostModel.findById(req.params.postId);

      // Pull out comment
      const comment = post.comments.find(
        (comment) => comment.id === req.params.comment_id
      );
      // If comment does not exist
      if (!comment) {
        return res.status(404).json({ msg: 'Comment does not exist' });
      }
      // Check user
      if (comment.user.toString() !== req.body.user.user.id) {
        return res.status(401).json({ msg: 'User not authorized' });
      }
      //get the removed index
      const removeIndex = post.comments
        .map((comment) => comment.user.toString())
        .indexOf(req.body.user.user.id);

      console.log(removeIndex);

      //remove one comment at the given index
      post.comments.splice(removeIndex, 1);

      await post.save();

      return res.json(post.comments);
    } catch (err: any) {
      console.error(err.message);
      return res.status(500).send('Server Error');
    }
  }
);

// @route    PUT api/posts/like/:id
// @desc     Like a post
// @access   Private
postRouter.put(
  '/like/:id',
  authMiddleware,
  async (req: Request, res: Response) => {
    try {
      //get the post by id
      const post = await PostModel.findById(req.params.id);

      // Check if the post has already been liked
      if (
        post.likes.filter(
          (like) => like.user.toString() === req.body.user.user.id
        ).length > 0
      ) {
        return res.status(400).json({ msg: 'Post already liked' });
      }

      post.likes.unshift({ user: req.body.user.user.id });

      await post.save();

      return res.json(post.likes);
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route    PUT api/posts/unlike/:id
// @desc     Unlike a post
// @access   Private
postRouter.put(
  '/unlike/:id',
  authMiddleware,
  async (req: Request, res: Response) => {
    try {
      const post = await PostModel.findById(req.params.id);
      console.log('testing');
      // Check if the post has not yet been liked by the user
      if (
        post.likes.filter(
          (like) => like.user.toString() === req.body.user.user.id
        ).length === 0
      ) {
        return res.status(400).json({ msg: 'Post has not yet been liked' });
      }

      //console.log(post.likes);
      console.log(
        post.likes.filter(
          (like) => like.user.toString() === req.body.user.user.id
        ).length
      );
      // console.log(req.body.user.user.id);

      // console.log(post.likes);
      //   //get the removed index
      const removeIndex = post.likes
        .map((like) => like.user.toString())
        .indexOf(req.body.user.user.id);

      //remove one like from the given index
      post.likes.splice(removeIndex, 1);

      await post.save();

      return res.json(post.likes);
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

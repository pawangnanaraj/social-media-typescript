import express, { Request, Response } from 'express';
import { authMiddleware } from '../../middleware/auth';
import { UserModel } from '../../models/User';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import config from 'config';
import { body, validationResult } from 'express-validator';

export const authRouter = express.Router();

// route --> get api/auth
// access --> public
//Authenticate the user and return the id
//this is a protected route
//console.log(auth);
authRouter.get('/', authMiddleware, async (req: Request, res: Response) => {
  try {
    //get only id and leave the password
    const userId = await UserModel.findById(req.body.user.user.id).select(
      '-password'
    );
    //console.log(userId);
    res.json(userId);
  } catch (error: any) {
    console.error(error);
    res.sendStatus(500).send('Server Error');
  }
});

// route --> post api/authts
// access --> public
//Authenticate the user and login
//this is a protected route

authRouter.post(
  '/',
  [
    body('email', 'Please enter a valid email').isEmail(),
    body('password', 'password id required').exists(),
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    //see if the user exits, send an error
    const { email, password } = req.body;
    try {
      let user = await UserModel.findOne({ email });
      if (!user) {
        return res.status(400).json({ errors: [{ msg: 'User not Found' }] });
      }

      //verify password

      const passwordMatch = await bcrypt.compare(password, user.password);

      if (!passwordMatch) {
        return res.status(400).json({ msg: 'Invalid Credentials' });
      }

      //return the JWT
      const payload = {
        user: {
          id: user.id,
        },
      };

      const token = jwt.sign(payload, config.get('jwtSecret'), {
        expiresIn: 360000,
      });
      res.json({ token: token });
    } catch (error: any) {
      console.error(error);
      return res.status(500).send('server error');
    }
  }
);

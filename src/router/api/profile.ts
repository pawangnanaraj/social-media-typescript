import express, { Request, Response } from 'express';
import { authMiddleware } from '../../middleware/auth';
import { check, validationResult } from 'express-validator';
import { ProfileModel } from '../../models/Profile';
import { PostModel } from '../../models/Posts';
import { UserModel } from '../../models/User';
import { Schema } from 'mongoose';
import Profile from '../../interfaces/Profile';
export const profileRouter = express.Router();

// @route    GET api/profile
// @desc     Get all profiles
// @access   Public
profileRouter.get('/', async (req: Request, res: Response) => {
  try {
    const profiles = await ProfileModel.find().populate('user', 'name');
    res.json(profiles);
  } catch (err: any) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route    POST api/profile
// @desc     Create or update user profile
// @access   Private
profileRouter.post(
  '/',
  authMiddleware,
  check('status', 'Status is required').notEmpty(),
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    // destructure the request
    const {
      location,
      status,
      bio,
      hobbies,
      facebook,
      linkedin,
      instagram,

      ...rest
    } = req.body;

    // console.log(req.body);
    // console.log(req.body.hobbies);
    // console.log(req.body.bio);
    // console.log(req.body.location);
    // build a profile
    const profileFields = {} as Profile;
    profileFields.user = req.body.user.user.id;
    if (location) profileFields.location = location;
    if (status) profileFields.status = status;
    if (bio) profileFields.location = bio;
    if (hobbies) {
      profileFields.hobbies = hobbies
        .split(',')
        .map((hobby: string) => hobby.trim());
    }
    // console.log(profileFields);

    // //profileFields.social ={};
    // if (facebook) {
    //   profileFields.social.facebook = facebook;
    // }
    // //if (linkedin) profileFields.social.linkedin = linkedin;
    // if (instagram) profileFields.social.instagram = instagram;
    // console.log('new');
    try {
      // Using upsert option (creates new doc if no match is found):
      let profile = await ProfileModel.findOne({ user: req.body.user.user.id });
      //update profile if found
      if (profile) {
        profile = await ProfileModel.findOneAndUpdate(
          { user: req.body.user.user.id },
          { $set: profileFields },
          { new: true }
        );
        return res.json(profile);
      }

      //if profile not found create a profile
      profile = new ProfileModel(profileFields);

      await profile.save();
      return res.json(profile);
    } catch (err: any) {
      console.error(err.message);
      return res.status(500).send('Server Error');
    }
  }
);

// @route    GET api/profile/me
// @desc     Get current users profile
// @access   Private
profileRouter.get(
  '/me',
  authMiddleware,
  async (req: Request, res: Response) => {
    try {
      const profile = await ProfileModel.findOne({
        user: req.body.user.user.id,
      }).populate('user', 'name');

      if (!profile) {
        return res
          .status(400)
          .json({ msg: 'There is no profile for this user' });
      }

      res.json(profile);
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route    GET api/profile/user/:user_id
// @desc     Get profile by user ID
// @access   Public
profileRouter.get('/user/:user_id', async (req: Request, res: Response) => {
  try {
    const profile = await ProfileModel.findOne({
      user: req.params.user_id,
    }).populate('user', 'name');

    if (!profile) {
      return res.status(400).json({ msg: 'Profile not found' });
    }

    return res.json(profile);
  } catch (err: any) {
    console.error(err.message);
    if (err.kind == 'ObjectId') {
      return res.status(400).json({ msg: 'Profile not found' });
    }

    return res.status(500).json({ msg: 'Server error' });
  }
});

// @route    DELETE api/profile
// @desc     Delete profile, user & posts
// @access   Private
profileRouter.delete(
  '/',
  authMiddleware,
  async (req: Request, res: Response) => {
    try {
      // Remove user posts
      // Remove profile
      // Remove user
      await Promise.all([
        PostModel.deleteMany({ user: req.body.user.user.id }),
        ProfileModel.findOneAndRemove({ user: req.body.user.user.id }),
        UserModel.findOneAndRemove({ _id: req.body.user.user.id }),
      ]);

      res.json({ msg: 'User deleted' });
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route    PUT api/profile/education
// @desc     Add profile education
// @access   Private
profileRouter.put(
  '/education',
  authMiddleware,
  check('school', 'School is required').notEmpty(),
  check('degree', 'Degree is required').notEmpty(),
  check('fieldofstudy', 'Field of study is required').notEmpty(),
  check('from', 'Please enter a date').notEmpty(),
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { school, degree, fieldofstudy, from, to, current, description } =
      req.body;

    const newEdu = {
      school,
      degree,
      fieldofstudy,
      from,
      to,
      current,
      description,
    };

    try {
      const profile = await ProfileModel.findOne({
        user: req.body.user.user.id,
      });

      profile.education.unshift(newEdu);

      await profile.save();

      res.json(profile);
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

//delete education
// @route    DELETE api/profile/education/:edu_id
// @desc     Delete education from profile
// @access   Private

profileRouter.delete(
  '/education/:edu_id',
  authMiddleware,
  async (req: Request, res: Response) => {
    try {
      const profile = await ProfileModel.findOne({
        user: req.body.user.user.id,
      });

      //remove index
      const removeIndex = profile.education
        .map((item) => item.id)
        .indexOf(req.params.edu_id);

      profile.education.splice(removeIndex, 1);

      await profile.save();
      return res.status(200).json(profile);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ msg: 'Server error' });
    }
  }
);

//adding social

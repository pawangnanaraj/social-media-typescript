import express, { Request, Response } from 'express';
import { UserModel } from '../../models/User';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import config from 'config';
import { body, validationResult } from 'express-validator';

export const registerUserRouter = express.Router();

// route --> POST  api/users
//register user
registerUserRouter.post(
  '/',
  [
    body('name', 'Name is required').not().isEmpty(),
    body('email', 'Please enter a valid email').isEmail(),
    body('password', 'please enter a valid with 6 or more alphanumeric value')
      .isLength({ min: 6 })
      .isAlphanumeric()
      .isAlphanumeric(),
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password } = req.body;

    try {
      let user = await UserModel.findOne({ email });

      //see if the user exits, send an error
      if (user) {
        return res
          .sendStatus(400)
          .json({ errors: [{ msg: 'user already exists' }] });
      } else {
        user = new UserModel({
          name,
          email,
          password,
        });

        //encrypt the password
        const salt = await bcrypt.genSalt(10);

        user.password = await bcrypt.hash(password, salt);

        await user.save();

        //return the JWT
        const payload = {
          user: {
            id: user.id,
          },
        };

        const token = jwt.sign(payload, config.get('jwtSecret'), {
          expiresIn: 360000,
        });
        res.json({ token: token });
      }
    } catch (error: any) {
      console.error(error.message);
      return res.sendStatus(500).send('server error');
    }
  }
);
